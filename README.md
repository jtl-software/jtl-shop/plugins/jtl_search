# Changelog

## 1.23.0
* Regelmäßige Full-Exports sind nun per Cronjob möglich
* Behebt ein Problem bei dem ggf. Bilder anderer Hersteller angezeigt wurden
* Behebt ein Problem wenn es keine Standard Wawi Sprache im Shop gibt

## 1.21.1
* Behebt ein Problem bei dem Exporte u.U. nicht möglich sind
* Kompatibilität zu PHP 8.2 hergestellt
* Export-Spracheinstellungen können nicht gespeichert werden behoben

## 1.21.0
* Kompatibilität zu JTL-Shop 5.2.0 hergestellt
* Zwei Probleme mit Umlauten in der Suche behoben
* Unvollständingen Export von Fremdsprachen behoben
* Ein Problem mit Kundengruppensichtbarkeiten behoben


## 1.20.2
* Darstellungsfehler im Evo-Template behoben
* Auswahl von Sortiermöglichkeiten in normalen Produktlisten behoben
* Sortierung wird auch ohne Neuladen einer Suchergebnisseite übernommen

## 1.20.1
* Fehler mit Sprachen in Shop 5 behoben
* Probleme beim Scrollen in mobiler Ansicht behoben
* JQuery und JQueryUI werden nicht mehr via CDN ausgeliefert

## 1.20.0
* Shop5-Kompatibilität
