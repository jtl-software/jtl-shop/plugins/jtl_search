<?php

declare(strict_types=1);

$plugin = $plugin ?? $oPlugin ?? \JTL\Plugin\Helper::getPluginById('jtl_search');
const JTLSEARCH_MANAGER_SERVER_URL = 'https://manager-jtlsearch.jtl-software.de/jtlsearch/manager/index.php';

// Pfade
define('JTLSEARCH_PFAD', $plugin->getPaths()->getBasePath());
define('JTLSEARCH_ADMIN_URL', $plugin->getPaths()->getAdminURL());

const JTLSEARCH_PFAD_INCLUDES  = JTLSEARCH_PFAD . 'includes/';
const JTLSEARCH_PFAD_CLASSES   = JTLSEARCH_PFAD . 'classes/';
const JTLSEARCH_PFAD_ADMINMENU = JTLSEARCH_PFAD . PFAD_PLUGIN_ADMINMENU;

const JTLSEARCH_ADMIN_CSS_URL_PATH = JTLSEARCH_ADMIN_URL . 'css/';
const JTLSEARCH_ADMIN_TPL_PATH     = JTLSEARCH_PFAD_ADMINMENU . 'template/';

const JTLSEARCH_PFAD_FRONTEND = JTLSEARCH_PFAD . PFAD_PLUGIN_FRONTEND;
const JTLSEARCH_PFAD_LIBS     = JTLSEARCH_PFAD . 'libs/';

define(
    'JTLSEARCH_URL_ADMINMENU_TESTPERIOD_TEMPLATES_CSS',
    $plugin->getPaths()->getAdminPath() . 'testperiod/templates/css/'
);
const JTLSEARCH_URL_ADMINMENU_TESTPERIOD_TEMPLATES_CSS_BASE =
    JTLSEARCH_URL_ADMINMENU_TESTPERIOD_TEMPLATES_CSS . 'base.css';

const JTLSEARCH_PFAD_ADMINMENU_VERWALTUNG           = JTLSEARCH_PFAD_ADMINMENU . 'verwaltung/';
const JTLSEARCH_PFAD_ADMINMENU_VERWALTUNG_INCLUDES  = JTLSEARCH_PFAD_ADMINMENU_VERWALTUNG . 'includes/';
const JTLSEARCH_PFAD_ADMINMENU_VERWALTUNG_TEMPLATES = JTLSEARCH_PFAD_ADMINMENU_VERWALTUNG . 'templates/';
const JTLSEARCH_PFAD_ADMINMENU_VERWALTUNG_CLASSES   = JTLSEARCH_PFAD_ADMINMENU_VERWALTUNG . 'classes/';

const JTLSEARCH_PFAD_ADMINMENU_SETTINGS           = JTLSEARCH_PFAD_ADMINMENU . 'settings/';
const JTLSEARCH_PFAD_ADMINMENU_SETTINGS_INCLUDES  = JTLSEARCH_PFAD_ADMINMENU_SETTINGS . 'includes/';
const JTLSEARCH_PFAD_ADMINMENU_SETTINGS_TEMPLATES = JTLSEARCH_PFAD_ADMINMENU_SETTINGS . 'templates/';
const JTLSEARCH_PFAD_ADMINMENU_SETTINGS_CLASSES   = JTLSEARCH_PFAD_ADMINMENU_SETTINGS . 'classes/';

// CURL Server Pass
const JTLSEARCH_SECRET_KEY = '5Ce5CSwQfq7JpAhB';

// Stat types
const JTLSEARCH_STAT_TYPE_VIEWED    = 1;
const JTLSEARCH_STAT_TYPE_BASKET    = 2;
const JTLSEARCH_STAT_TYPE_RECOMMEND = 3;
const JTLSEARCH_STAT_TYPE_WISHLIST  = 4;
const JTLSEARCH_STAT_TYPE_DEMAND    = 5;
const JTLSEARCH_STAT_TYPE_NOTIFY    = 6;
const JTLSEARCH_STAT_TYPE_COMPARE   = 7;

// Export-Limit pro Run
const JTLSEARCH_LIMIT_N_METHOD_1 = JOBQUEUE_LIMIT_M_EXPORTE;
const JTLSEARCH_LIMIT_N_METHOD_2 = 100;
const JTLSEARCH_LIMIT_N_METHOD_3 = 500;

// Deltaexport
const JTLSEARCH_DELTAEXPORT_ITEMS_MAX = 500;

define('JTLSEARCH_URL_EXPORTFILE_DIR', JTL\Shop::getURL() . '/' . PFAD_EXPORT);
const JTLSEARCH_PFAD_EXPORTFILE_DIR = PFAD_ROOT . PFAD_EXPORT;

const JTLSEARCH_URL_EXPORTFILE_ZIP  = JTLSEARCH_URL_EXPORTFILE_DIR . 'jtlsearch.zip';
const JTLSEARCH_PFAD_EXPORTFILE_ZIP = JTLSEARCH_PFAD_EXPORTFILE_DIR . 'jtlsearch.zip';

define('JTLSEARCH_URL_DELTA_EXPORTFILE_ZIP', JTLSEARCH_URL_EXPORTFILE_DIR . 'delta_jtlsearch' . time() . '.zip');
define('JTLSEARCH_PFAD_DELTA_EXPORTFILE_ZIP', JTLSEARCH_PFAD_EXPORTFILE_DIR . 'delta_jtlsearch' . time() . '.zip');

// Max Anzahl an Datensätzen pro Datei
const JTLSEARCH_FILE_LIMIT       = 5000;
const JTLSEARCH_FILE_NAME        = 'export_';
const JTLSEARCH_FILE_NAME_SUFFIX = '.jtl';

const JTLSEARCH_PRODUCT_EXCLUDE_ATTR = 'nosearch';
const JTLSEARCH_NO_SSL               = false;
